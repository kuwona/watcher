/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"reflect"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	apierrs "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	watchv1alpha1 "gitlab.com/kuwona/watcher/api/v1alpha1"
)

// WatcherReconciler reconciles a Watcher object
type WatcherReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

func ignoreNotFound(err error) error {
	if apierrs.IsNotFound(err) {
		return nil
	}
	return err
}

// +kubebuilder:rbac:groups=watch.apburgoon.com,resources=watchers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=watch.apburgoon.com,resources=watchers/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=core,resources=configmaps,verbs=get;update;patch

// Reconcile runs to get Watcher to desired state
func (r *WatcherReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	ctx := context.Background()
	log := r.Log.WithValues("watcher", req.NamespacedName)

	var watcher watchv1alpha1.Watcher
	if err := r.Get(ctx, req.NamespacedName, &watcher); err != nil {
		log.Error(err, "unable to find Watcher")
		return ctrl.Result{}, ignoreNotFound(err)
	}

	// Get the previously created Watcher ConfigMap if it exists
	cfgKey := types.NamespacedName{
		Name:      fmt.Sprintf("%s-config", watcher.Name),
		Namespace: watcher.Namespace,
	}

	constructConfigMap := func(w *watchv1alpha1.Watcher, cfgName types.NamespacedName) (*corev1.ConfigMap, error) {
		cfg := &corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name:      cfgName.Name,
				Namespace: cfgName.Namespace,
			},
			Data:       w.Spec.ConfigData,
			BinaryData: w.Spec.ConfigBinaryData,
		}

		if err := ctrl.SetControllerReference(w, cfg, r.Scheme); err != nil {
			return nil, err
		}

		return cfg, nil
	}
	cfg, err := constructConfigMap(&watcher, cfgKey)
	if err != nil {
		log.Error(err, "unable to create ConfigMap from Watcher")
		return ctrl.Result{}, err
	}

	shouldUpdate := false
	createErr := r.Create(ctx, cfg)
	if apierrs.IsAlreadyExists(createErr) {
		shouldUpdate = true
	}

	if shouldUpdate {
		var configMap corev1.ConfigMap
		if err := r.Get(ctx, cfgKey, &configMap); err != nil {
			log.Error(err, "unable to get ConfigMap")
			return ctrl.Result{}, ignoreNotFound(err)
		}

		if !reflect.DeepEqual(configMap, cfg) {
			retryErr := retry.RetryOnConflict(retry.DefaultRetry, func() error {
				var result corev1.ConfigMap
				if err := r.Get(ctx, cfgKey, &result); err != nil {
					return err
				}
				result.Data = cfg.Data
				result.BinaryData = cfg.BinaryData
				updateErr := r.Update(ctx, &result)
				return updateErr
			})
			if retryErr != nil {
				return ctrl.Result{}, retryErr
			}
		}
	}

	watcher.Status.ConfigMapName = cfgKey.Name
	if err := r.Status().Update(ctx, &watcher); err != nil {
		log.Error(err, "unable to update Watcher status")
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets controller up with the manager
func (r *WatcherReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&watchv1alpha1.Watcher{}).
		Complete(r)
}
