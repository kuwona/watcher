/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"time"

	watch "gitlab.com/kuwona/watcher/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("WatcherController", func() {
	const timeout = time.Second * 30
	const interval = time.Second * 1

	BeforeEach(func() {
		// Add any setup steps that needs to be executed before each test
	})

	AfterEach(func() {
		// Add any teardown steps that needs to be executed after each test
	})

	// Add Tests for OpenAPI validation (or additional CRD features) specified in
	// your API definition.
	// Avoid adding tests for vanilla CRUD operations because they would
	// test Kubernetes API server, which isn't the goal here.
	Context("Watcher", func() {
		It("should create successfully", func() {
			key := types.NamespacedName{
				Name:      "watcher-sample",
				Namespace: "default",
			}

			created := &watch.Watcher{
				ObjectMeta: metav1.ObjectMeta{
					Name:      key.Name,
					Namespace: key.Namespace,
				},
				Spec: watch.WatcherSpec{
					Container: corev1.Container{
						Name:  "watcher-test",
						Image: "busybox",
					},
					Volume: corev1.Volume{
						Name: "watcher-volume",
					},
					ConfigData: map[string]string{
						"test":  "testing",
						"test2": "WTF",
					},
				},
			}

			// Create
			Expect(k8sClient.Create(context.Background(), created)).Should(Succeed())

			By("Expecting a ConfigMap generated")
			Eventually(func() map[string]string {
				c := &corev1.ConfigMap{}
				_ = k8sClient.Get(context.Background(), types.NamespacedName{Name: fmt.Sprintf("%s-config", key.Name), Namespace: key.Namespace}, c)
				return c.Data
			}, timeout, interval).Should(Equal(map[string]string{"test": "testing"}))
		})
	})
})
